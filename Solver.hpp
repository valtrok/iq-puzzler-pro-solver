#include <unordered_set>

#include <Board.hpp>
#include <Piece.hpp>

class Solver {
public:
  Solver(Board, std::vector<Piece>);

  bool solve();
  void place(const Piece &, const Coordinates);
  Board getBoard() const;

private:
  Solver copyWithPiecePlaced(const Piece &, const Coordinates);

  Board m_board;
  std::unordered_map<uint8_t, std::vector<Piece>> m_remaining_pieces;
};
