#include <Board.hpp>
#include <bits/stdint-uintn.h>

Coordinates Coordinates::transform(const Coordinates &coordinates) const {
  return {static_cast<int8_t>(x + coordinates.x), static_cast<int8_t>(y + coordinates.y)};
}

bool Coordinates::operator==(const Coordinates &coordinates) const {
  return x == coordinates.x and y == coordinates.y;
}

Board::Board(uint8_t size_x, uint8_t size_y)
    : size_x{size_x}, size_y{size_y} {
  empty_slots.reserve(size_x * size_y);
  placed_pieces.reserve(size_x * size_y);
  for (int8_t x = 0; x < size_x; ++x) {
    for (int8_t y = 0; y < size_y; ++y)
      empty_slots.insert({x, y});
  }
};

void Board::place(const Piece &piece, const Coordinates coordinates) {
  for (const auto dot : piece.shape) {
    const auto transformed = coordinates.transform(dot);
    empty_slots.erase(transformed);
    placed_pieces.insert(std::make_pair(transformed, piece.color));
  }
}
