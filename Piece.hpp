#pragma once

#include <vector>
#include <unordered_set>
#include <string>
#include <optional>

struct Coordinates;
struct Board;

using Shape = std::vector<Coordinates>;

struct Piece {
  Piece(Shape, uint8_t);

  std::optional<Coordinates> fitsIn(const Board&) const;

  Piece getRotatedCopy() const;
  Piece getMirroredCopy() const;
  std::vector<Piece> getAllShapes() const;

  Shape shape;
  uint8_t color;
};
