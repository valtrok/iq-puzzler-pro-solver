#include <iostream>
#include <vector>

#include <Board.hpp>
#include <Piece.hpp>
#include <Solver.hpp>

int main() {
  auto board = Board(11, 5);

  const auto pieces = std::vector<Piece>{
      Piece{{{0, 0}, {0, 1}, {1, 1}}, 44},                  // 0. cyan
      Piece{{{0, 0}, {0, 1}, {1, 1}, {0, 2}}, 159},         // 1. dark green
      Piece{{{0, 0}, {0, 1}, {1, 1}, {2, 1}}, 17},          // 2. dark blue
      Piece{{{0, 0}, {0, 1}, {1, 1}, {1, 2}}, 218},         // 3. red
      Piece{{{0, 0}, {0, 1}, {1, 1}, {1, 2}, {1, 3}}, 164}, // 4. pink
      Piece{{{0, 0}, {0, 1}, {1, 1}, {1, 2}, {2, 2}}, 225}, // 5. purple
      Piece{{{0, 0}, {0, 1}, {1, 1}, {0, 2}, {1, 2}}, 49},  // 6. light cyan
      Piece{{{0, 0}, {0, 1}, {1, 1}, {0, 2}, {0, 3}}, 220}, // 7. yellow
      Piece{{{0, 0}, {0, 1}, {1, 1}, {2, 1}, {1, 2}}, 202}, // 8. orange
      Piece{{{0, 0}, {0, 1}, {1, 1}, {2, 1}, {2, 0}}, 106}, // 9. green
      Piece{{{0, 0}, {0, 1}, {1, 1}, {2, 1}, {3, 1}}, 124}, // 10. light red
      Piece{{{0, 0}, {0, 1}, {0, 2}, {1, 2}, {2, 2}}, 110}, // 11. blue
  };

  auto solver = Solver(board, pieces);

  // Level 1
  solver.place(pieces[4].getMirroredCopy(), {0, 4});  // pink
  solver.place(pieces[11].getMirroredCopy(), {0, 2}); // blue
  solver.place(pieces[1], {2, 1});                    // dark green
  solver.place(pieces[0].getMirroredCopy(), {3, 1});  // cyan
  solver.place(pieces[7].getMirroredCopy().getRotatedCopy(), {4, 4}); // yellow
  solver.place(pieces[3].getMirroredCopy(), {4, 2});                  // red
  solver.place(pieces[8].getRotatedCopy().getRotatedCopy().getRotatedCopy(),
               {6, 2});               // orange
  solver.place(pieces[2], {6, 3});    // dark blue
  solver.place(pieces[6].getRotatedCopy().getRotatedCopy().getRotatedCopy(),
               {8, 0}); // light cyan

  std::cout << "Before solve:\n" << solver.getBoard();

  if (solver.solve())
    std::cout << "After solve:\n" << solver.getBoard();
  else
    std::cout << "Solution not found\n";
}
