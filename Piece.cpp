#include <algorithm>

#include <Piece.hpp>
#include <Board.hpp>

Piece::Piece(Shape base_shape, uint8_t color) : shape{base_shape}, color{color} {}

Piece Piece::getMirroredCopy() const {
  auto new_shape = Shape{};
  std::transform(std::begin(shape), std::end(shape), std::back_inserter(new_shape),
                 [](auto coord) -> Coordinates {
                   return {coord.x, static_cast<int8_t>(-coord.y)};
                 });
  return Piece{new_shape, color};
}

Piece Piece::getRotatedCopy() const {
  auto new_shape = Shape{};
  std::transform(std::begin(shape), std::end(shape), std::back_inserter(new_shape),
                 [](auto coord) -> Coordinates {
                   return {coord.y, static_cast<int8_t>(-coord.x)};
                 });
  return Piece{new_shape, color};
}

std::vector<Piece> Piece::getAllShapes() const {
  auto all_shapes = std::vector<Piece>{};
  all_shapes.reserve(8);
  // All rotations of base shape
  all_shapes.push_back(*this);
  for (auto i = 0; i < 3; ++i)
    all_shapes.push_back(all_shapes.back().getRotatedCopy());

  // All rotations of mirrored shape
  all_shapes.push_back(all_shapes.back().getMirroredCopy());
  for (auto i = 0; i < 3; ++i)
    all_shapes.push_back(all_shapes.back().getRotatedCopy());

  return all_shapes;
}

std::optional<Coordinates> Piece::fitsIn(const Board &board) const {
  const auto fits = [this, &board](auto coordinates) {
    for (const auto dot : shape) {
      if (not board.empty_slots.contains(coordinates.transform(dot)))
        return false;
    }
    return true;
  };

  for(const auto empty_slot : board.empty_slots) {
    if (fits(empty_slot))
      return {empty_slot};
  }

  return std::nullopt;
}
