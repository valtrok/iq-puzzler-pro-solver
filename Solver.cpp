#include "Piece.hpp"
#include <Solver.hpp>
#include <bits/stdint-uintn.h>
#include <unordered_map>

Solver::Solver(Board board, std::vector<Piece> initial_pieces)
    : m_board{board} {
  for (const auto &piece : initial_pieces) {
    m_remaining_pieces.insert(
        std::make_pair(piece.color, piece.getAllShapes()));
  }
}

bool Solver::solve() {
  if (m_remaining_pieces.empty())
    return true;
  for (const auto [color, piece_shapes] : m_remaining_pieces) {
    for (const auto piece : piece_shapes) {
      const auto coordinates = piece.fitsIn(m_board);
      if (coordinates) {
        auto solver_copy = copyWithPiecePlaced(piece, *coordinates);
        if (solver_copy.solve()) {
          m_board = solver_copy.getBoard();
          m_remaining_pieces.clear();
          return true;
        }
      }
    }
  }
  return false;
}

void Solver::place(const Piece &piece, const Coordinates coordinates) {
  m_board.place(piece, coordinates);
  m_remaining_pieces.erase(piece.color);
}

Board Solver::getBoard() const { return m_board; };

Solver Solver::copyWithPiecePlaced(const Piece &piece,
                                   const Coordinates coordinates) {
  auto new_solver = Solver{*this};
  new_solver.place(piece, coordinates);
  return new_solver;
}
