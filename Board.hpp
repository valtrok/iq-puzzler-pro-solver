#pragma once

#include <bits/stdint-uintn.h>
#include <iostream>
#include <unordered_map>

#include <Piece.hpp>

struct Piece;

struct Coordinates {
  Coordinates transform(const Coordinates &) const;

  bool operator==(const Coordinates &) const;

  int8_t x;
  int8_t y;
};

namespace std {
template <> struct hash<Coordinates> {
  size_t operator()(const Coordinates &coordinates) const {
    return (coordinates.x << 8) | coordinates.y;
  }
};
} // namespace std

struct Board {
  Board(uint8_t size_x, uint8_t size_y);

  void place(const Piece &, const Coordinates);

  friend std::ostream &operator<<(std::ostream &os, const Board &board) {
    for (int8_t y = -1; y <= board.size_y; ++y) {
      if (y == -1)
        os << "╭─";
      else if (y == board.size_y)
        os << "╰─";
      else
        os << "│ ";
      for (int8_t x = 0; x < board.size_x; ++x) {
        if (y > -1 and y < board.size_y) {
          const auto coordinates = Coordinates{x, static_cast<int8_t>(board.size_y - y - 1)};
          if (board.placed_pieces.contains(coordinates)) {
            os << "\033[38;5;" << int{board.placed_pieces.at(coordinates)}
               << "m⬤ "
               << "\033[0m";
          } else {
            os << "  ";
          }
        } else {
          os << "──";
        }
      }
      if (y == -1)
        os << "─╮";
      else if (y == board.size_y)
        os << "─╯";
      else
        os << " │";
      os << "\n";
    }
    return os;
  };

  uint8_t size_x;
  uint8_t size_y;
  std::unordered_set<Coordinates> empty_slots;
  std::unordered_map<Coordinates, uint8_t> placed_pieces;
};
